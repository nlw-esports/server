import express, {Request, Response} from "express";
const API_PORT = 3333
const app = express();

app.get('/ads', (req: Request, res: Response) => {
    return res.json([
        { id: 1, name: 'Anuncio 1'},
        { id: 2, name: 'Anuncio 2'},
        { id: 3, name: 'Anuncio 3'},
        { id: 4, name: 'Anuncio 4'},
    ])
});

app.listen(API_PORT, () => {
    console.log(`server is listening to port: ${API_PORT}`)
})